## 两大类问题

1. 回归问题：预测的为**值**，模型输出的为**实数值**。如：预测房价。

2. 分类问题：预测的为**类别**，模型输出的为**概率分布**。

## 目标函数

1. 目标函数的作用：衡量构建的模型的好
2. 对于分类问题，目标函数需要衡量目标类别与当前预测的差距

如：三分类问题输出例子：**预测值的分布为[0.2,0.7,0.1]**，模型预测样本为第1类。但假设实际目标类别（真实类别）为第2类，则三分类的**真实类别的表示为 2**。将 2 经过**one_hot编码**，生成1个长度为3的向量（第2个位置是1，其余位置都是0 ），即 **2 → one_hot → [0,0,1]** ，**则[0,0,1]即为真实值的分布**。

**注：** One_hot编码：用于把正整数变为向量表达。输入为正整数，**输出为1个长度不小于正整数的向量**。而在**分类问题上，one_hot 会生出1个只有在正整数位置处为1，其余位置均为0的向量。**

分类问题中常见的两种目标函数:

- **平方差损失**：![image-20210504155810314](README.assets/image-20210504155810314.png)

注： y和Model(x)都是向量，所以这里做的是一个向量的运算。

例：预测值：[0.2,0.7,0.1]；真实值：[0,0,1]；损失函数值**(给预测值的分布和真实值的分布计算距离)**： [(0.2-0)²+(0.7-0)²+(0.1-1)²] 0.5 = 0.67

- **交叉熵损失**：![image-20210504155822175](README.assets/image-20210504155822175.png)

3.对于回归问题需要衡量预测值与实际值的差距

回归问题中常见的两种目标函数：

- 均方损失： ![image-20210505122204233](README.assets/image-20210505122204233.png)
- 平均绝对值损失： ![image-20210505122212883](README.assets/image-20210505122212883.png)



# 实战-分类问题



## **1.各种库的引入**

```import matplotlib as mpl
import matplotlib.pyplot as plt
%matplotlib inline # 在python console里面生成图像
import numpy as np
import sklearn
import pandas as pd
import os
import sys
import time
import tensorflow as tf
 
from tensorflow import keras
 
print(tf.__version__)  #打印tensorflow的版本
print(sys.version_info)
for module in mpl,np,pd,sklearn,tf,keras:
    print(module.__name__,module.__version__)
```

## **2.导入关于分类问题的数据集**

```
"""对于分类问题，导入一个分类问题的数据集。在keras中有一定的数据集。"""
fashion_mnist = keras.datasets.fashion_mnist  #导入数据集
"""
mnist是手写数字的数据集。fashion_mnist中有一些黑白图像，可在keras.datasets中直接导入。
"""
 
(x_train_all,y_train_all),(x_test,y_test) = fashion_mnist.load_data()  #使用load_data函数把训练集和测试集都拆分出来

x_valid,x_train = x_train_all[:5000],x_train_all[5000:]  #把训练集再次拆分为验证集和训练集

y_valid,y_train = y_train_all[:5000],y_train_all[5000:]
 
# 打印验证集、训练集和测试集的shape
print(x_valid.shape,y_valid.shape)  #5000张图片，28*28的图像
print(x_train.shape,y_train.shape)  #55000张图片，28*28的图像
print(x_test.shape,y_test.shape)  #10000张图片，28*28的图像
```

## **3.了解数据集**

```
#输出一个图片
def show_single_image(img_arr):  #定义一个“展示一张图像”函数
    plt.imshow(img_arr,cmap="binary")   #调用imshow
"""cmap="binary"定义的是颜色图谱，默认是rgb。这里的样本本身是黑白图片，rgb足以满足。"""
    plt.show() # 调用show，显示图像
    
show_single_image(x_train[0])
```

结果为：

![image-20210504151637267](README.assets/image-20210504151637267.png)

或者显示部分数据集

```
"""
函数show_imgs,可显示n行n列。显示图像一般有参数x_data即可，但是y_data可把图像的类别也显示出来。此外，还需定义一个类别名的输出。
"""
# 显示所有图
def show_imgs(n_rows, n_cols, x_data, y_data, class_names):  #定义函数show_imgs

    #做一些验证
    assert len(x_data) == len(y_data)  #验证x的样本数和y的样本数是一样的
    assert n_rows * n_cols <len(x_data)  #验证行和列的乘积不能大于样本数，否则样本数不够用
 
    # 定义一张大图，图的大小分别是n_cols*1.4和n_rows*1.6，1.4和1.6是缩放因素
    plt.figure(figsize = (n_cols * 1.4,n_rows * 1.6)) 
 
    for row in range(n_rows):
        for col in range(n_cols):
            index = n_cols * row + col  #计算当前位置上放置的图片的索引
            plt.subplot(n_rows, n_cols, index+1)  #使用plt在这张大图上画一张子图，
            plt.imshow(x_data[index],cmap="binary",  #调用imshow
                      interpolation='nearest')  #interpolation='nearest' 缩放图片的差值的方法，'nearest'表示使用最近的像素点表示整个像素点的值
            plt.axis('off')  #关闭坐标系，把小图放到大图中，就无需坐标系了
            plt.title(class_names[y_data[index]])  #给每张小图都配置对应的名字title
    plt.show()  #show展示出图
    
class_names = ['T-shirt','Trouser','Pullover','Dress',
               'Coat','Sandal','Shirt','Sneaker',
              'Bag','Ankle boot']  #十个类
 
#调用函数，显示大图，显示一个三行五列的15张图片以及其对应的类别
show_imgs(3, 5, x_train, y_train, class_names) 

```

结果为：

![image-20210504151729273](README.assets/image-20210504151729273.png)

## **4.构建模型**

```
#tf.keras.models.Sequential()构建模型
"""
在官网中对Sequential的介绍为"layers:list of layers to add to model."即将一系列的层次堆叠起来。
"""
 
model = keras.models.Sequential()  #创建一个Sequential的对象
 
    
"""
往这个Sequential对象里面添加层，首先添加的是输入层，这一层的作用：使用Flatten将输入的对象（1张28*28的图像）展平，
即将1个28*28的二维矩阵展平成为1个一维向量。
"""
model.add(keras.layers.Flatten(input_shape=[28,28]))  #添加输入层，展开输入的图片

    
"""
展平之后，再加入新的一层，这一层为全连接层（最普通的神经网络），
并把上一层的所有单元都与下一层的所有单元进行一一连接。把这一层的单元数设为300，activation即为激活函数，设为"relu"。
"""
model.add(keras.layers.Dense(300, activation="relu"))  #添加全连接层，单元数为300

#这一层的100个单元就会和上一层300个单元进行全连接
model.add(keras.layers.Dense(100, activation="relu"))  #添加全连接层，单元数为100

 
    
"""再添加一层，控制其的输出。上述的问题是一个分类问题，模型输出应为长度为10的一个概率分布，
这里应使其的输出为一个长度为10的向量。
"""
model.add(keras.layers.Dense(10,activation="softmax"))  #添加输出层，输出概率分布

 
"""
#Sequential函数的另一种写法，不用逐层调用add函数，可以将每层都放置在一个列表中：
model = keras.models.Sequential([keras.layers.Flatten(input_shape=[28,28]),
                                keras.layers.Dense(300, activation="relu"),
                                keras.layers.Dense(100, activation="relu"),
                                keras.layers.Dense(10, activation="softmax")])
效果与上相同。
"""
 
#有了概率分布之后，就可以计算目标函数
model.compile(loss = "sparse_categorical_crossentropy",
             optimizer = "adam",
             metrics = ["accuracy"]) 
"""
这个函数的参数分别为：
    损失函数 loss，
    "crossentropy"即为上述“交叉熵”的损失函数。加上前缀sparse的原因为：y → index。
    放置损失函数的前提是：y → one_hot → []（向量）。因为这里的y长度等于样本数量，所以对于每个样本而言，y都是1个数值。若y为1个数值，则使用"sparse_categorical_crossentropy"；若y为1个向量，就无需使用"sparse"，直接使用"categorical_crossentropy"即可。
    
    optimizer为目标函数的求解方法，调整参数，使目标函数越来越小，这里就是目标函数参数的调整方法。
    除目标函数的指标之外，还应注意其他指标，如"accuracy"。
    目的是将目标函数指标和其他我们关心的指标构建到图中。
"""
```

**2种激活函数如下：**

relu函数：![image-20210504155853778](README.assets/image-20210504155853778.png) ，即输入一个x，输出是x与0的最大值。当x>0时，输出的y为x；当x<0时，输出的y为0。
softmax函数：该函数将向量变成概率分布。

如：存在向量 ![image-20210504155931440](README.assets/image-20210504155931440.png)，转变为 ![image-20210504155948097](README.assets/image-20210504155948097.png)，![image-20210504160001655](README.assets/image-20210504160001655.png)。 这样就将向量变成了一个概率分布，因为y中的3个值的和为1，且都在0和1之间。

## **5.查看模型中的层数**

```
model.layers  # 可以看到我们构建的模型中有多少层
```

输出结果为:

```
[<tensorflow.python.keras.layers.core.Flatten at 0x22b9b16ef70>,
 <tensorflow.python.keras.layers.core.Dense at 0x22b9b0d0cd0>,
 <tensorflow.python.keras.layers.core.Dense at 0x22b9aeda7c0>,
 <tensorflow.python.keras.layers.core.Dense at 0x22b9b0e2d00>]
```

如上所示，正是构建的4层



## **6.参数状况**

通过`model.summary()`输出模型各层的参数状况

```
model.summary()
```

![image-20210504152138264](README.assets/image-20210504152138264.png)

**Outshape：**

第一层的  **[None,784]** （样本数为784的1个矩阵），变化为第二层的 **[None,300]** （样本数为300的1个矩阵）。

变化过程为： **[None,784] * W（变化的方式是乘以一个矩阵）+ b（加一个偏置）= [None,300]**
其中： W.shape为[784,300]，b=[300]（b为长度为300的一个向量）  所以第二层的参数量=784*300+300=235500（参数量的计算）。

## **7.训练模型**

```
#构建好图以后，可以开启训练
history = model.fit(x_train, y_train, epochs=10,
          #epochs表示命令这个训练集多少次，epochs=10表示命令这个训练集10次
         validation_data=(x_valid,y_valid)
          #每隔段时间，会对这个训练集做验证 
         )

#history表示model.fit返回的一个值，称为 数据结果
```



结果为：

![image-20210504152303990](README.assets/image-20210504152303990.png)

## **8.查看history的变量类型**

type(history)  *#查看history变量类型：callbacks*

结果为：

![img](README.assets/2020020801153618.png)

## **9.history中的值**

history中的一个变量，其中包含一些值

```
history.history
```

结果为：

![image-20210504152452499](README.assets/image-20210504152452499.png)

## **10.可视化**

主要是：打印出指标的变化过程

```
def plot_learning_curves(history):
    
    """
    #DataFrame是一个重要的数据结构（k值做列名，v值做值）， 
    figsize=(8,5)表示把图的大小设置为8和5 
    """
    pd.DataFrame(history.history).plot(figsize=(8, 5))  
    
    plt.grid(True)  #显示网格
    plt.gca().set_ylim(0,1)  #设置坐标轴的范围，set_ylim(0,1)设置y轴坐标轴的范围
    plt.show()  #显示这张图
    
plot_learning_curves(history)
```

效果为:

![img](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAeMAAAEzCAYAAAACSWsXAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjIsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+WH4yJAAAgAElEQVR4nO3deXxU9aH//9dnluwLYUvYN0F2BAKoKEZRQIt6XUGRItfqw9qq1dpae7v4u9rlurRf21qt9sqiKFJFr1uVUo1U6kK0WHZEZF9kCZCQTDLL5/fHTIZJSMgAQ04yeT8fj3mccz7nc875zMlk3mebc4y1FhEREXGOy+kGiIiItHYKYxEREYcpjEVERBymMBYREXGYwlhERMRhCmMRERGHNRrGxphnjDFfG2NWNjDeGGN+Z4zZYIz5tzFmROKbKSIikrzi2TOeDUw6xviLgb6R1y3AEyffLBERkdaj0TC21i4B9h+jyuXAXBv2EdDGGNMpUQ0UERFJdok4Z9wF2BozvC1SJiIiInHwJGAepp6yeu+xaYy5hfChbNLT00d269YtAYsPC4VCuFy6Hq0paF03Da3npqH13DS0nsPWr1+/11rboW55IsJ4GxCbql2BHfVVtNY+BTwFUFhYaEtKShKw+LDi4mKKiooSNj9pmNZ109B6bhpaz01D6znMGLO5vvJEbKa8BnwzclX1mcBBa+3OBMxXRESkVWh0z9gY8wJQBLQ3xmwDfg54Aay1TwJvAZcAG4AKYOapaqyIiEgyajSMrbXXNTLeAt9JWItERERaGZ1NFxERcZjCWERExGEKYxEREYcpjEVERBymMBYREXGYwlhERMRhCmMRERGHKYxFREQcpjAWERFxmMJYRETEYQpjERERhymMRUREHKYwFhERcZjCWERExGEKYxEREYcpjEVERBymMBYREXGYx+kGiIgkvVAQgn4I+SPdQMxwIKa87nCg/uls8Mi8rY1ZkD3OchooP975NF7ebcsGWPo52FCkjg13Y/ux9YwPNTCeRsY3NP1xLvOKP0FKRv3rLIEUxk0tFAr/I4WCkQ9ATTcUGRf7CtYePpnxCZx3l21fwsfrwLjAmHCXSLfel6nTbaDOMedR86LxOtRTx9Z9f6FG/gbBBurYo8tCwcbnHapn3dadrtZwkN5btoL/vSPrBmLWU2Nd4qvX6DyPZ5mR+nW/6Oori35hx/QfVXa886hbRuPzAHpt+gqqF8cXgscMy2OEamxYtVJ9ADYezxQx3xs1n6/od42pZzyNjK9v+jimid3wOYWSI4z3f0XPr56H4D+OBF0oGBN6sd1QPeWhBuqHwv9sjdatb571lQecXlMJ0Rdgg9OtSFLGHd2A6BoKwQ7TeEjJCQp/2XbDwI4UcHvB5Yl0veD2RLp1y73gSY2vXrzza7BeA9MZ95GNn5r3Eu09FeU0UB7/fP7xj39w7rjzOGbwRYdbn+QI4wOb6bn5RdjiCn9IXe7wh9a4wRVTFu266gw3VM8NnrT46za6LE+duq7a9ep7HWvccY93x3zgT3Tehg8+WMo5Y8+uZ2/TNl5GY3XqG1+nvN55HGs+wZj3H/vezJGy+tZTtKxunfqmqxl21ZlPA+ux3nnX/hJaUlxMUVFR45//eg+7xdOl/rITmlekG30P9ewxx7tnHi2rO+5E5tHQnv4JrGc5KUFPBqRkOt2MZis5wrjXeRSf9ypF55/vdEtahYA3CzLaOt0MqWHMUQEjIi1LclxNrS8jERFpwZIjjEVERFowhbGIiIjDkuOcsbRK1lqsz0fo8GFCFRXhbn39FRWY1DTcubm427SJvCL9OTkYj/4NkoG1FltVRaiyEltZSaiyklClD1tZQcjnI1RRifUdKQ9VVmArfWRt3syeFSsxaam4UtPC3bQ0TGoarrTUI920NExqKq7UcH9N17jdTr/1JmWtxfr92Kqq8P9fVTW2uqa/ChsZDvl8tfrT165j/44dGK8X4/GGu14vxuuJlB3pUqtOnfGxdZLo9KS+haTJ2FCIUEXl0aFZcZjQ4YrGQ/Wo6SrCPx87Sa7s7CNBXbfbpu5wuN+Vk4Nx6cDS8bDB4HEHZaiykpCvEltRGa4X21+rjg9bWRlzlXh8jNdLhrXsXbToxN+Y13tUQEe7kTCPBn1q6tFljYV/rXHhFwCBQDgIq3zhjZCqqnBA1tfvqwqHYt3+yPTRfp+PUHVD/dXReR7vegbIAXaf+Fqun+foII92U7wQG/p160RC/sg86tRLCffnTZ+Oq2adn0JJEcaVq1aR98ijbJ49J7yV6nFj3J6G+z1uiJTF9tce31B/Y+Pd4T+m+0jd8LxrymLHR7aoa36aEgphY39eEgr/RMfayA0Moj/ZsTHlMcOhELVudFAzv5qyxuYXqnOHmpqymJ+u2FCI1OXLOXjoUIMhGTx8GHu4gmBFbHkFtqIi7r+pSU/HlZGBKzMz/MrIwN02D2+3rkfK64x3ZWbirq88I4OQz0fw4EGCBw4QPBDpHqzbDY+r3raV4IGDhA4davhLxxjcOTm427TBVTewo90j4e3OC/e7MjObxda8DYWw1dX17NnUs5dTFW9/dThofb7aoevzYSsqsH7/cbfTpKfjSkvDlZ4e7o8Mu/Pa4O3UKVKehis9A1d6WqR+Oq6M9HAYRspjpzdpaeHPRWoqxuuluLiY88aNCweXzxezxxfp+qrC7y8yLuTzhQOtKmZcTcjVlEWmD1VUEio9EBmOTBcZR/Akbibhcp30hqip2WBISYn0p+JKSY32u7Ozj5SnpmJS4uiPvOrrX/rRR4wdMwYbCIT3rP01XT8Ewt3ouEAAWx07HKkXW8cfqDNNTZ065TH9IV9l/XXq1CXms9pm6lRQGMfJAi5XeEVWVmKDQWwwCIFApD8AgeBR/UfGh/slPm2AHbEFxkRDLzYEvR3zGwxNV2ZNoMb0x4Rnog/9uVNScOfkQLducU9jg0GChw4ROiq06wn1vfuo/nIjwQMHCJWXNzxTjyccznHsiXvXraPc5TrqcN9R/VVV4S/6evurjoRLzZ6Nz3dCwViL11vvl7grLRyI3tzcY4ZjXKGZmtpkRx+MyxVdblOxfn+DgV+7rOrI3m9NfzAU3lOOrvdI+KWk1tqDPhKKabhSU6IBbBw4xGszM/G0b9+kyzxR1tpwPvj9mCb6TCRFGKcPHkTp3Xcx7CR+uG9r9iRjQzoQgLrBHghCsL7+SN1AJPAbqlt3PIDLhP8xYm9O4Ap3o+XRW0HWlLmO/KTLhL9MYqc3LhPzk6945hc7T8JlrpjbxMUs47NVqxg9blw0PE16elIesjVuN568PMjLO67prN9P8NChhoM7puvftQvf2rUEDx486shBW2DrsRbkch1zz8aVkYE7L+/o8hPcs6nV38rOk54KxuvF7fVCVpbTTZE6jDHhc9Jeb5MtMynCOBGMMZHDzW5ISXG6Oc1aoKyMlJ49nW5Gs2W8Xjzt2uFp1+64pgtVV0eCO/xavnw5I8aMaXAvB4+nWRzyFpGTpzAWaSZcKSm4OnbE27EjAP6KCtKHDXO4VSLSFJLv2KKIiEgLozAWERFxmMJYRETEYQpjERERhymMRUREHKYwFhERcZjCWERExGEKYxEREYcpjEVERBymMBYREXGYwlhERMRhCmMRERGHKYxFREQcpjAWERFxWFxhbIyZZIxZZ4zZYIz5UT3jc40xrxtjPjfGrDLGzEx8U0VERJJTo2FsjHEDjwMXAwOB64wxA+tU+w6w2lo7DCgCHjXGpCS4rSIiIkkpnj3j0cAGa+1Ga201MB+4vE4dC2QbYwyQBewHAgltqYiISJLyxFGnC7A1ZngbMKZOnT8ArwE7gGxgirU2VHdGxphbgFsA8vPzKS4uPoEm16+8vDyh85OGaV03Da3npqH13DS0no8tnjA29ZTZOsMTgeXABUAf4G/GmH9Yaw/Vmsjap4CnAAoLC21RUdFxN7ghxcXFJHJ+0jCt66ah9dw0tJ6bhtbzscVzmHob0C1muCvhPeBYM4GFNmwD8BXQPzFNFBERSW7xhPEyoK8xplfkoqyphA9Jx9oCjAcwxuQDpwMbE9lQERGRZNXoYWprbcAY813gHcANPGOtXWWMuTUy/kngAWC2MWYF4cPa91pr957CdouIiCSNeM4ZY619C3irTtmTMf07gAmJbZqIiEjroDtwiYiIOExhLCIi4jCFsYiIiMMUxiIiIg5TGIuIiDhMYSwiIuIwhbGIiIjDFMYiIiIOUxiLiIg4TGEsIiLiMIWxiIiIwxTGIiIiDlMYi4iIOExhLCIi4jCFsYiIiMMUxiIiIg5TGIuIiDhMYSwiIuIwhbGIiIjDFMYiIiIOUxiLiIg4TGEsIiLiMIWxiIiIwxTGIiIiDlMYi4iIOExhLCIi4jCFsYiIiMMUxiIiIg5TGIuIiDhMYSwiIuIwhbGIiIjDFMYiIiIOUxiLiIg4TGEsIiLiMIWxiIiIwxTGIiIiDlMYi4iIOExhLCIi4jCFsYiIiMMUxiIiIg5TGIuIiDhMYSwiIuIwhbGIiIjDFMYiIiIOUxiLiIg4LK4wNsZMMsasM8ZsMMb8qIE6RcaY5caYVcaY9xPbTJHW5WDVQapD1U43Q0SaiKexCsYYN/A4cBGwDVhmjHnNWrs6pk4b4I/AJGvtFmNMx1PVYJFkFAgF+Peef/PB9g9YumMpq/etxoOH+e/MZ3TBaMZ0GsPg9oPxurxONzUp7K3cS8muEkp2l3Cg9ADDq4aTm5rrdLOkFWs0jIHRwAZr7UYAY8x84HJgdUyd64GF1totANbarxPdUJFks+vwLpZuX8rSHUv5aMdHlPnLcBs3QzsM5bYzbmPtxrXsrN7JH5f/kceXP066J52R+SMZUzCG0Z1Gc3re6bhdbqffRovwdcXXlOwqYdnuZZTsKmHToU0AZHgyqAxU8tErH3Hr0FuZcvoUvG5t8EjTiyeMuwBbY4a3AWPq1OkHeI0xxUA28Ji1dm5CWiiSJKqCVXy6+1OWbl/KP3f8kw0HNgCQn5HPhJ4TGNtlLGM6jSEnJQeA4tJiioqKOOA7QMnuEj7a+RGf7PqERz99FICclBxGFYxiTKcxjCkYQ6/cXhhjHHt/zcnuw7sp2V3Csl3LKNldwuZDmwHI8mYxIn8EV/W9isKCQvq37c+Li1/kfd7nf5b9D8+vfZ67Rt7Fhd0v1LqUJmWstceuYMw1wERr7bciw9OB0dba22Pq/AEoBMYD6cCHwDestevrzOsW4BaA/Pz8kfPnz0/YGykvLycrKyth85OGaV3Hx1rLnsAe1lSuYbVvNV/4vsBv/Xjw0CetDwPSBzAwbSAF3oJ6v/gbWs8HAwdZ71sffe0P7gcgx51Dv9R+9EvrR7/0frTztDvl77G5KA2UssG3gS+qvmCDbwN7AnsASDfp9Enrw2lpp9E3tS9dU7riMrUvlSkvLyczM5M1vjW8WvoqO/076Z3amyvyrqBnak8H3k1y0vdG2Pnnn/+ptbawbnk8YXwWcL+1dmJk+D4Aa+2vYur8CEiz1t4fGf5f4G1r7V8amm9hYaEtKSk5gbdSv+Li8F6EnHpa1w2r8Ffwya5Pwud+ty9lW/k2AHrk9GBs57GM7TKWwvxCMrwZjc4rnvVsrWVb+TY+2fkJH+/6mE92fsI+3z4AumR1YUynMYwuGM3ogtF0yOhw0u+vudhZvjO657ts17Loes5OyWZk/khG5Y+isKAwrkP5ses5EArw6oZX+cO//sA+3z4u7nkxd4y4g67ZXU/1W0p6+t4IM8bUG8bxHKZeBvQ1xvQCtgNTCZ8jjvV/wB+MMR4ghfBh7N+eXJNFmj9rLetL17N0x1KWbl/KZ19/RiAUIN2TzpiCMcwYNIOxncfSLafbKVm+MYZu2d3olt2Nq/pdhbWWjQc38vHOj/l458f8bfPfWPjFQgB65/aOHtIuLChsURcs7SjfET3kvGzXMraXbwfCh+oL8wu5fsD1jCoYRd82fU/qPLrH5eHqfldzca+LmbVyFnNWzWHxlsVMGzCNm4feHD2FIJJojYaxtTZgjPku8A7gBp6x1q4yxtwaGf+ktXaNMeZt4N9ACPiztXblqWy4iFMOVh3kw50fhs/9bv8nX1eGr1fsm9eX6QOmM7bLWIZ3HE6KO6XJ22aMoU+bPvRp04frB1xPMBRkbena6J7zqxte5YW1L2Aw9G/bPxzOncYwouOIuPbWm8r28u3Rvd5Pd38aDd/c1FwK8wuZPnA6hfmF9M3re9Rh50TI9Gby3eHf5Zp+1/D7f/2eOavm8MqGV/j2sG9zbb9rdZGXJFw8e8ZYa98C3qpT9mSd4YeBhxPXNJHmIRgKsnrfaj7YET70vGLvCkI2RHZKNmd3Ppuxncdyduezyc/Md7qpR3G73AxqN4hB7QYxc/BM/EE/K/auiB7SnrdmHrNXzcZjPAzpMCT6M6phHYY12cZEzaH2mp8aLdu1jJ2HdwKQl5pHYUE4fEcVjOK0NqedkvBtSH5mPg+e8yA3DLyBR0oe4def/JoX1r7AXSPu4oLuF+giL0mYuMJYpLXZW7k3/LOj7Uv5585/crDqIAbD4PaDuWXoLYztPJbB7QfjcbWsfyGv28uI/BGMyB/Bt4d9m8pAJcu/Xs7HOz/mk12f8PSKp/nTv/9EqjuV4R2HR885D2w3MGHv1VrLtrJtLNu9LHroedfhXQC0TWvLyPyRzBw8k8L8Qvq06dOk4duQ/m378/RFT/OP7f/g0ZJH+V7x9xjRcQQ/GPUDBrcf7HTzJAm0rG8SkVPEH/SzfM/y6O9+1+5fC0C7tHac1/U8xnYey1mdzyIvLc/hliZWuiedszqfxVmdzwKgrLqMT3d/Gj7nvOtjHvvsMSB82LYwvzC653w8h4ettWwp2xL9ne+yXcv4uiJ8aL9tWltGFYzipsE3MapgFL1zezfbvU1jDOO6juPszmez8IuFPL78ca578zou6XUJd464k85ZnZ1uorRgCmNptbaXb4/u/X6862MO+w/jMR7O6HgGd464k3O6nEO/vH7NYs+sqWSnZFPUrYiibkUA7Kvcx7Ldy/hk5yd8susT3t8WvtNtXmpe9DfOowtG0yOnRzRErbVsPrQ5eoONkl0l0fPq7dLaMapgFKMKRlGYX9gifxvtcXm49vRruaTXJTyz8hnmrp7L4s2LmTZwGjcPuZnslGynmygtkMJYWg1fwEfJ7hKWbl/KB9s/iN6FqXNmZy7pdUn4phsFY8hK0W8ha7RLb8eknpOY1HMSEL5rWM0h7Y92fsSizYuA8I1LRheMJhAKULK7hD2V4d/5dkjvQGFBIYX5hYwqGEXPnJ4tLnwbkpWSxR0j7uDa06/l9//6PbNWzuLVL17l1mG3cs3p1+jWpXJcFMaS1A77D/Pal6/x/tb3KdldQlWwilR3KoX5hVx7+rWM7TKWXjktb+/MKQWZBVx+2uVcftrl0cPPNeG8dMdSPMZTa883do85WRVkFvCLc37BtAHTeKTkEX71ya/CF3mNvIvzu52f9O9fEkNhLElpT8Ue5q2Zx4L1CyirLqNnTk+u6XcN53Q5h5H5I0nzpDndxBbPGEOPnB70yOnBtadf63RzHDew3UD+d8L/8v6293m05FHufO9OCvMLuafwHga1H+R086SZUxhLUvnywJfMWTWHNza+QdAGGd99PDcOupGhHYY63TRpBYwxFHUrYmyXsSxcv5A/fv5Hpr45lcm9J3PH8DvolNXJ6SZKM6UwlhbPWkvJ7hJmr5rNkm1LSHOncWXfK5kxcMYpu/OVyLF4XV6m9J/CJb0jF3mtmsvfNv+N6QOnc9Pgm3RdghxFYSwtViAU4O9b/s7slbNZuW8leal53DbsNqb2n5p0P0GSlik7JZs7R9zJtf2u5Xf/+h1/XvFnFn6xkNuG3cZV/a5qcb9Tl1NHnwRpcSr8Fby64VXmrp7L9vLtdM/uzk/P/CmX9rmUdE+6080TOUqnrE786txfccOAG3i45GEe/PhB5q2dx90j7+a8rufpIi9RGEvLsa9yHy+sfYH56+ZzsOogQzsM5Z7Cezi/2/kn9XAAkaYyqP0gZk2cxXtb3+O3n/6W29+9ndEFo/l+4fcZ2G6g080TBymMpdnbdHATc1fP5bUvX6MqWEVRtyJmDprJ8I7DtUchLY4xhgu6X8C5Xc/lL+v+whOfP8GUN6Zwae9LuWPEHRRkFjjdRInwBXxN9ssLhbE0W8u/Xs6sleG9CK/Ly6V9LmXGoBn0yu3ldNNETprX5eX6AddzaZ9LeXrF08xbPY9FmxfxzYHf5KYhN5HpzXS6ia3Wuv3rmLNqDkt3LOWtK99qkr+FwlialZAN8d7W95i9cjbL9ywnJyWHbw35FtcPuJ726e2dbp5IwmWnZHP3yLuZcvoUHvvsMZ5e8TQvf/Ey3znjO1zZ90pd5NVErLUs3bGU2atm8/HOj0n3pHNl3yvxB/3QBDdT019ZmgVfwMdrX77G3NVz2XxoM12yuvCj0T/iitOuaFbP2RU5VbpkdeGhcQ8xfcB0Hil5hAc+eoB5a+bx/cLvc26Xc3VK5hSpDlbz5sY3mbt6LhsObKBjeke+N+J7XN3vanJTc5usHUkRxtXBasqCZU43Q07AAd8B5q+bzwtrX2C/bz8D2w3k4XEPc2GPC7VHIK3SkA5DmD1pNu9ueZfffPobvvP37zCm0xjuKbyH/m37O928pFHqK2XBugW8sPYF9vn2cXre6fzynF8yqeckvO6mv694Unzb/XvPv/nxth/z6IuP0i+vH6e3PZ1+ef3ol9eP3rm9HVmxcmxby7by7OpneXXDq1QGKjmnyznMHDSTUQWjtAcgrZ4xhvE9xjOu6zgWrF/AE58/wbWvX8tlfS7j9uG3k5+Z73QTW6xNBzfx7Opnee3L1/AFfZzT5RxmDJrBmIIxjn73JEUYd83uypV5VxJsG2R96XqeX/M81aFqADzGQ682vaLhfHpeOKjbp7fXl74DVu5dyayVs1i8ZTEu4+Ibvb7BjEEz6JvX1+mmiTQ7XreXaQOmhS/y+vfTzFszj3c2vcOU08N39xrQdoC+x+JgreWzrz9jzqo5FG8txuPycGmfS5k+YDqn5Z3mdPOAJAnjgswCzs85n6JzioDwnZk2H9rM+tL10VfJrhLe3PhmdJq81LxwQLftFw3qPm36kOpOdehdJK+QDfHB9g+YtXIWJbtLyPJmMWPQDKb1n6YtfJE45KTk8P3C7zPl9Cn87rPf8dya55izeg5ds7oyoecEJvScwMC2AxXMdQRCARZvXsycVXNYuW8lbVLbcMvQW5jaf2qzuyA0KcK4Lo/LQ582fejTpg8X97o4Wn6w6mCtgF6/fz1/WfcXfEEfAG7jpmdOz6NCOj8jXx/yE1BzYcScVXP48uCX5Gfkc0/hPVzV9yrdm1fkBHTN7spD5z3Efb77eHfLuyzavIg5q+bwzMpn6JLVhQk9wsE8qN2gVv2dVV5dzsIvFjJvzTx2HN5Bj5we/GTMT7jstMua7V36kjKMG5Kbmht91mqNYCjIlrIttQL68z2f89dNf43WyUnJOepcdJ82fZrtH9Vph6oPsWDdAp5f8zx7KvfQL69f+MKIXpP0wHWRBMhLy+OqfldxVb+rOOA7wHtb3+Odze/w7OpnmbVqFl2yunBRj4uY0GMCg9sPbjXBvOvwLuatmcdL61+i3F/OiI4juHf0vRR1K8JlXE4375haVRjXx+1y0yu3F71yezGx58Ro+aHqQ2wo3cC60nXRoF74xUIqA5UAuIyL7tndj5yLjgR1p8xOreaDX9fO8p08u+ZZXl7/MhWBCs7sdCYPjn2Qszqf1WrXicip1iatDVf0vYIr+l7BwaqD0T3m59Y8x+xVs+mc2TkczD0nMKT9kKT8X1y9bzVzVs1h0aZFWCwX9biIGYNmMLj9YKebFrdWH8YNyUnJYUT+CEbkj4iWhWyIbWXbouG8bv86Vu9bzaLNi6J1sr3Z9M3rW+tQd982fZP6t7Jr969l1spZvLPpHQAm9ZrEjYNu1M8wRJpYbmpurWAu3lrMos2LmLd2HnNWz6EgsyC6xzy0w9Bmv7d4LCEb4h/b/sGc1XNYtmsZmd5MrhtwHTcMuIHOWZ2dbt5xUxgfB5dx0T2nO91zunNhjwuj5Yf9h/mi9Ita56Nf3/g6h9cdBsBg6JbdLboXXRPSXbK6tNh/BmstH+74kFmrZvHRzo/I8GQwbcA0bhhwgx6gLtIM5Kbmcvlpl3P5aZdzqPpQOJg3LWL+2vk8u/pZ8jPyuajHRUzsObFFBbMv4OP1ja/z7Opn+ergV+Rn5PP9kd/nqn5XkZ2S7XTzTpjCOAEyvZmc0fEMzuh4RrQsZEPsKN9R+4Kx0vX8fcvfsVgA0j3pZHgy8Lq9eF3hl8flifZ73V48xlNrfK067nqmqWd8vfOtM020Tp1pPC5PrX9Sf8jPJ+Wf8PvXf8/60vV0SO/A90Z8j2tOv4aclJwmX/ci0riclBwu63MZl/W5jLLqsmgwv7juRZ5b8xwdMzoyoccELupxEWd0PKNZBvO+yn28uO5FXlz3Ivt9+xnQdgC/PvfXTOg5ISmuRVEYnyIu46Jrdle6Znflgu4XRMsr/BVsOLCB9aXr+fLAl/iCPvxBP/6Qn0AogD/kj74CoQCHg4fxVx09PhAK4A/6Cdgj3VOlZoPA4/IQDAWpCFTQJ7cP/332f/ON3t8gxZ1yypYtIomVnZLNpX0u5dI+l1JWXcb7295n0aZFLFi3IBzM6R25sMeFTOg5geEdhzsezBsPbmTuqrm8/uXrVIeqOa/recwYNIPC/MKkOv+tMG5iGd4MhnYYytAOQxM635ANEQgFagd6sP6Arxl3VFnNNJGAr298MBQkd38ut118m+P/pCJycrJTspncezKTe0+mvLo8GswvrX+J59c+T4f0DuFg7hEO5qZ6bri1lmW7ljFn9RyWbFtCqjuVy067jOkDp9M7t3eTtKGpKYyThMu4SHGnNMleanFxsYJYJMlkpWTxjd7f4Bu9v8Fh/2He3/o+izYvYuEXC3lh7Qu0T2/P+O7jmdhzIiM6jjglwewP+Xln0zvMXTWXNfvX0DatLbcNu40p/afQNq1twrfKJEoAAByGSURBVJfXnCiMRUSklkxvJpf0voRLel9Chb+CJduWsGjzIv5vw//x4roXaZfWLrrHPDJ/5EkHc1l1GS+tf4l5a+axu2I3vXJ78fOzfs7k3pNJ86Ql6F01bwpjERFpUIY3g0m9JjGp16RwMG9fwqJNR4K5bVpbLuwePsc8Mn/kcT1tbXv5dp5b/RwLv1hIRaCC0QWj+dlZP+OcLue0uqNvCmMREYlLhjeDST0nMalnOJj/sf0f/G3z33h94+ssWL+AtmltGd99PBf1uIhRBaMaDOYVe1YwZ/UcFm9eDMDEnhOZMWgGA9sNbMq306wojEVE5LhleDOY2HMiE3tOpDJQyQfbP2DRpkW8sfEN/rL+L+Sl5nFB9wuY0HMCowtGE7Ih/r7l78xdNZfPvv6MLG8W0wdOZ9qAaRRkFjj9dhynMBYRkZOS7knnoh4XcVGPi6gMVLJ0+1IWbVrEW1+9xctfvEyb1DZ4gh72btlL58zO/KDwB1zZ90o9MCaGwlhERBIm3ZPOhT0u5MIeF+IL+Fi6IxzMG3Zu4N6z7+XCHhce13nl1kJrRERETok0Txrju49nfPfxFBcXU9SryOkmNVut63I1ERGRZkhhLCIi4jCFsYiIiMMUxiIiIg5TGIuIiDhMYSwiIuIwhbGIiIjDFMYiIiIOUxiLiIg4TGEsIiLisLjC2BgzyRizzhizwRjzo2PUG2WMCRpjrk5cE0VERJJbo2FsjHEDjwMXAwOB64wxRz10MlLvf4B3Et1IERGRZBbPnvFoYIO1dqO1thqYD1xeT73bgZeBrxPYPhERkaQXTxh3AbbGDG+LlEUZY7oAVwBPJq5pIiIirUM8j1A09ZTZOsP/D7jXWhs0pr7qkRkZcwtwC0B+fj7FxcVxNrNx5eXlCZ2fNEzrumloPTcNreemofV8bPGE8TagW8xwV2BHnTqFwPxIELcHLjHGBKy1r8ZWstY+BTwFUFhYaIuKik6w2UcrLi4mkfOThmldNw2t56ah9dw0tJ6PLZ4wXgb0Ncb0ArYDU4HrYytYa3vV9BtjZgNv1A1iERERqV+jYWytDRhjvkv4Kmk38Iy1dpUx5tbIeJ0nFhEROQnx7BljrX0LeKtOWb0hbK298eSbJSIi0nroDlwiIiIOUxiLiIg4TGEsIiLiMIWxiIiIwxTGIiIiDlMYi4iIOExhLCIi4jCFsYiIiMMUxiIiIg5TGIuIiDhMYSwiIuIwhbGIiIjDFMYiIiIOUxiLiIg4TGEsIiLiMIWxiIiIwxTGIiIiDlMYi4iIOExhLCIi4jCFsYiIiMMUxiIiIg5TGIuIiDhMYSwiIuIwhbGIiIjDFMYiIiIOUxiLiIg4TGEsIiLiMIWxiIiIwxTGIiIiDlMYi4iIOMzjdANEpOXyB0NUVAeprA5SUR3A63aRl5lCZoobY4zTzRNpMRTGJ8laSzBkCVkIRfqD1mJDEIwMW2tj+jlSx1pSPW7SvG7SU9ykeVx43DpYIYkVClkq/UEOVwcioRkOzopIf2V13XFHxteMiw3c2Omqg6F6l5nidpGX6SUvI4W2mSnkZabQNqOm6w13M1Oi49tmppDmdTfxmhFpPpIijNfsPMRjn/l4dtOycCDa8BdQKBqURwdmKFInaMPjo/0hak0XioRn7bpE+61N7HtJcbtI9bpIjwR0ujcc1mkxZWneI+VHl4XrpUWmja2XluKKDntbaejbyN/M1vRDZDhSHttfpw4x40J1piVav57pG5tvrXKin6uNB4KkbNgbDj9/kMrqAIerglT6w6F4uCoSkDHjavpjQ9Xnrz8wG5LidpGR6iYj8tnKTPWQ7nXTPiuFjJQMMlLcZKS4SU/xRPszIv3VwRClh6vZX1Ed7h72U1pRzZodh9hfUc3BSn+D/zPpXnckuGNC/KgwPzKuTYaXVI8CXJJDUoRxVSDE3kpL9SEfbpfBGIPbEO33uFykegwul8FlwG1i+l0Glwm/avrdLsJlLhOua4j2R+fvCs/HRMrC5UTruEzM/GvmWzPPyPwBqgMhfP4gPn+ISn/4i7ayOhgpiwz7Q/iqg+wtr46W+SL1Kv1BQiewQeBxmXpC2xXdS68pS40G/pEg3/CVn7Xmy9obKZFuoKYsVPtIQSgUM67ORlL0VbOBVHeamHqxG1OBUCi8YVSnDcHg0cs4kXXULHz0cb3Fbpchw+sOh2ZKOCwzU93kpnvplJMWDsiYceFhT3iamv7I3zmjJnBTwgF8Ko/OBEOWg5V+9h+uprSiOtytJ7z3H65my/4K9h+upswXaHB+Wake8jK9MXvdKXX2umvvnbdJ9+rokzRLSRHGZ3RrwwNj0ykqOtfppjQ5ay3+oD0qoGuHdijaH1unMrIRULes9HA1O6IbBqHovIKxibZu7VFt8bhMrY2Wmo2R6Cu6MXKk3+OK2RByHdmIchkTPkJg6kwbs/ETu/HkjpmPO7YNMRtUGEOkg8FEupFhY+ovjwwDeI2lV7qPdFcoXKGOY50hPfbpU1Pv9H6/n9RU75E2GYMrpr3xC0Ze1bWLKsOvKsKvppYFZLmgezaQDeE1kBJ5HWHtkaNaociGVdBayvyGj7+G3WV+SivCAb/h63JKD1dzuDrY4HJz072RsA53Kw9WUXxoFVmpHrLSPGSleshOC7+yUr11hj0KczklkiKMWzNjDCkeQ4rHRW6695Quyx8Mh/rSDz7gvHHnHgm/SFAmu6+++ors7HzatWvXJBcnlZWVkZ2dfcqX0xJZa9m3bx+92pfRq1evo8b7/EEOVNTZA6+1J+6n9HA12w/4+Lo0yMrS7ZT5/HEdQUn3uslK85AdCe+akM5K9dYK7fA4b7ReNNRTvWSmntojEE3BRo6EVQdC+IMhqgMhqmr6gyH8AUt1MEh1wFIdDLFiT4CcLaXkpHnISfOSneYlzevShX4RCmOJm9ftwut2ke4xZKS0vo+Oz+ejZ8+e+vJoBowxtGvXjj179tQ7Ps3rpiDXTUFuWqPzKi4upqioCGvDR5jKfQHKqgKU+QKU+wKUV/kp80WGq8Kv8LA/POwLsLesIlIeLos31LPTPNFgz07z1to7z0mr6fdGQ78m2FPcrnoDLzYY6w5HAzNmOn/NdDXT1KkfG67hMhudtz8YOu5rZh799J+1hr1uQ3aal5zIhktOenhjJSc9MpwW3sDJSY90I8O56TUbPl7cSbIj0Pq+UUVOgoK4+Uj038IYE7kQzUPHk5iPtZaK6mA0tKMhXU/Il1cFOBQdDrCnrIoyn5+ySOgn+gJRgBSPi1S3C6/HRYrbhddjSHG7SPG4SXGHj7KleFxkpXnwul1H6kf6a7o1daPDkf7UmrKYZaR4XHz22WecNnAIZb4AhyrDGziHfH7KfH4OVYbX0SFfgD1l5dHhY51uqJGZ4j4qrGsPx4Z7uJsbE/bNZe9cYSzSgmRlZVFeXu50M+QYjDFkpnrITPWQn3Pi8wmFLBWRPfW6e+fVgVAkEGsHXko0ME2t4Zqg9EQuQHVC2Vduik4/vs2cQDAU3mCpDAd3OLzrhnnt4b3l1WzcezhaHmjkMIXHZY4O85hQv/uifmSmnvqoVBiLiDRDLpeJnIv2AI0fbk9GHreLNhkptMlIabxyPay1+Pyh6B74wZg98Np75LVDfePe8ujw9yf0S/C7qp/CWKQFstbywx/+kL/+9a8YY/jJT37ClClT2LlzJ1OmTOHQoUMEAgGeeOIJzj77bG666SZKSkowxvCf//mf3HXXXU6/BZFTzhgT/qlmipv8nOa9QaMwFjkB/9/rq1i941BC5zmwcw4/v3RQXHUXLlzI8uXL+fzzz9m7dy+jRo1i3LhxPP/880ycOJH/+q//IhgMUlFRwfLly9m+fTsrV64E4MCBAwltt4icvJZ9bb1IK/XBBx9w3XXX4Xa7yc/P57zzzmPZsmWMGjWKWbNmcf/997NixQqys7Pp3bs3Gzdu5Pbbb+ftt98mJ+ckTmSKyCmhPWORExDvHuypYhu4zHbcuHEsWbKEN998k+nTp/ODH/yAb37zm3z++ee88847PP744yxYsIBnnnmmiVssIseiPWORFmjcuHG8+OKLBINB9uzZw5IlSxg9ejSbN2+mY8eO3Hzzzdx000189tln7N27l1AoxFVXXcUDDzzAZ5995nTzRaQO7RmLtEBXXHEFH374IcOGDcMYw0MPPURBQQFz5szh4Ycfxuv1kpWVxdy5c9m+fTszZ84kFAo/MOJXv/qVw60XkbriCmNjzCTgMcAN/Nla++s646cB90YGy4FvW2s/T2RDRYTob4yNMTz88MM8/PDDtcbPmDGDGTNmHDWd9oZFmrdGD1MbY9zA48DFwEDgOmPMwDrVvgLOs9YOBR4Ankp0Q0VERJJVPOeMRwMbrLUbrbXVwHzg8tgK1tp/WmtLI4MfAV0T20wREZHkFc9h6i7A1pjhbcCYY9S/CfhrfSOMMbcAtwDk5+dTXFwcXyvjUF5entD5ScNa67rOzc2lrKysyZYXDAabdHktkc/nO+nPYmv9PDc1redjiyeM67uRab2/qzDGnE84jM+pb7y19ikih7ALCwttUVFRfK2MQ82TV+TUa63res2aNU36SEM9QrFxaWlpDB8+/KTm0Vo/z01N6/nY4gnjbUC3mOGuwI66lYwxQ4E/Axdba/clpnkiIiLJL55zxsuAvsaYXsaYFGAq8FpsBWNMd2AhMN1auz7xzRQREUleje4ZW2sDxpjvAu8Q/mnTM9baVcaYWyPjnwR+BrQD/hh5PFfAWlt46potIiKSPOL6nbG19i3grTplT8b0fwv4VmKbJiJOCQQCeDy6J5BIU9HtMEVamP/4j/9g5MiRDBo0iKeeCv+k/+2332bEiBEMGzaM8ePHA+GrV2fOnMmQIUMYOnQoL7/8MgBZWVnReb300kvceOONANx4443cfffdnH/++dx777188sknnH322QwfPpyzzz6bdevWAeGrvO+5557ofH//+9/z97//nSuuuCI637/97W9ceeWVTbE6RJKCNn1FTsRffwS7ViR2ngVD4OJfN1rtmWeeoW3btlRWVjJq1Cguv/xybr75ZpYsWUKvXr3Yv38/AA888AC5ubmsWBFuZ2lp6bFmC8D69etZvHgxbrebQ4cOsWTJEjweD4sXL+bHP/4xL7/8Mk899RRfffUV//rXv/B4POzfv5+8vDy+853vsGfPHjp06MCsWbOYOXPmya0PkVZEYSzSwvzud7/jlVdeAWDr1q089dRTjBs3jl69egHQtm1bABYvXsz8+fOj0+Xl5TU672uuuQa32w3AwYMHmTFjBl988QXGGPx+f3S+t956a/Qwds3ypk+fznPPPcfMmTP58MMPmTt3boLesUjyUxiLnIg49mBPheLiYhYvXsyHH35IRkYGRUVFDBs2LHoIOZa1lsgFlbXElvl8vlrjMjMzo/0//elPOf/883nllVfYtGlT9DeiDc135syZXHrppaSlpXHNNdfonLPIcdA5Y5EW5ODBg+Tl5ZGRkcHatWv56KOPqKqq4v333+err74CiB6mnjBhAn/4wx+i09Ycps7Pz2fNmjWEQqHoHnZDy+rSpQsAs2fPjpZPmDCBJ598kkAgUGt5nTt3pnPnzjz44IPR89AiEh+FsUgLMmnSJAKBAEOHDuWnP/0pZ555Jh06dOCpp57iyiuvZNiwYUyZMgWAn/zkJ5SWljJ48GCGDRvGe++9B8Cvf/1rJk+ezAUXXECnTp0aXNYPf/hD7rvvPsaOHUswGIyWf+tb36J79+4MHTqUYcOG8fzzz0fHTZs2jW7dujFwYN1nyYjIsRhr672z5SlXWFhoS0pKEjY/3Wqt6bTWdb1mzRoGDBjQZMtribfD/O53v8vw4cO56aabmmR5ifibtNbPc1PTeg4zxnxa3304dFJHRBJi5MiRZGZm8uijjzrdFJEWR2EsIgnx6aefOt0EkRZL54xFREQcpjAWERFxmMJYRETEYQpjERERhymMRUREHKYwFklSsU9nqmvTpk0MHjy4CVsjIseiMBYREXGYfmcscgL+55P/Ye3+tQmdZ/+2/bl39L0Njr/33nvp0aMHt912GwD3338/xhiWLFlCaWkpfr+fBx98kMsvv/y4luvz+fj2t79NSUkJHo+H3/zmN5x//vmsWrWKmTNnUl1dTSgU4uWXX6Zz585ce+21bNu2jWAwyE9/+tPo7TdF5MQpjEVaiKlTp/K9730vGsYLFizg7bff5q677iInJ4e9e/dy5plnctlll9X7VKWGPP744wCsWLGCtWvXMmHCBNavX8+TTz7JnXfeybRp06iuriYYDPLWW2/RuXNn3nzzTSD8MAkROXkKY5ETcKw92FNl+PDhfP311+zYsYM9e/aQl5dHp06duOuuu1iyZAkul4vt27eze/duCgoK4p7vBx98wO233w5A//796dGjB+vXr+ess87iF7/4Bdu2bePKK6+kb9++DBkyhHvuuYd7772XyZMnc+65556qtyvSquicsUgLcvXVV/PSSy/x4osvMnXqVObNm8eePXv49NNPWb58Ofn5+Uc9o7gxDT0s5vrrr+e1114jPT2diRMn8u6779KvXz8+/fRThgwZwn333cd///d/J+JtibR62jMWaUGmTp3KzTffzN69e3n//fdZsGABHTt2xOv18t5777F58+bjnue4ceOYN28eF1xwAevXr2fLli2cfvrpbNy4kd69e3PHHXewceNG/v3vf9O/f3/atm3LDTfcQFZWVq3nHIvIiVMYi7QggwYNoqysjC5dutCpUyemTZvGpZdeSmFhIWeccQb9+/c/7nnedttt3HrrrQwZMgSPx8Ps2bNJTU3lxRdf5LnnnsPr9VJQUMDPfvYzli1bxg9+8ANcLhder5cnnnjiFLxLkdZHYSzSwqxYsSLa3759ez788MN665WXlzc4j549e7Jy5UoA0tLS6t3Dve+++7jvvvtqlU2cOJGJEyeeQKtF5Fh0zlhERMRh2jMWSWIrVqxg+vTptcpSU1P5+OOPHWqRiNRHYSySxIYMGcLy5cudboaINEKHqUVERBymMBYREXGYwlhERMRhCmMRERGHKYxFktSxnmcsIs2LwlhETqlAIOB0E0SaPf20SeQE7PrlL6lak9jnGacO6E/Bj3/c4PhEPs+4vLycyy+/vN7p5s6dyyOPPIIxhqFDh/Lss8+ye/dubr31VjZu3AjAE088QefOnZk8eXL0Tl6PPPII5eXl3H///RQVFXH22WezdOlSLrvsMvr168eDDz5IdXU17dq1Y968eeTn51NeXs7tt99OSUkJxhh+/vOfc+DAAVauXMlvf/tbAJ5++mnWrFnDb37zm5NavyLNmcJYpIVI5POM09LSeOWVV46abvXq1fziF79g6dKltG/fnv379wNwxx13cN555/HKK68QDAYpLy+ntLT0mMs4cOAA77//PgClpaV89NFHGGP485//zEMPPcSjjz7KAw88QG5ubvQWn6WlpaSkpDB06FAeeughvF4vs2bN4k9/+tPJrj6RZk1hLHICjrUHe6ok8nnG1lp+/OMfHzXdu+++y9VXX0379u0BaNu2LQDvvvsuc+fOBcDtdpObm9toGE+ZMiXav23bNqZMmcLOnTuprq6mV69eACxevJj58+dH6+Xl5QFwwQUX8MYbbzBgwAD8fj9Dhgw5zrUl0rIojEVakJrnGe/ateuo5xl7vV569uwZ1/OMG5rOWtvoXnUNj8dDKBSKDtddbmZmZrT/9ttv5+677+ayyy6juLiY+++/H6DB5X3rW9/il7/8Jf3792fmzJlxtUekJdMFXCItyNSpU5k/fz4vvfQSV199NQcPHjyh5xk3NN348eNZsGAB+/btA4geph4/fnz0cYnBYJBDhw6Rn5/P119/zb59+6iqquKNN9445vK6dOkCwJw5c6LlEyZM4A9/+EN0uGZve8yYMWzdupXnn3+e6667Lt7VI9JiKYxFWpD6nmdcUlJCYWEh8+bNi/t5xg1NN2jQIP7rv/6L8847j2HDhnH33XcD8Nhjj/Hee+8xZMgQRo4cyapVq/B6vfzsZz9jzJgxTJ48+ZjLvv/++7nmmms499xzo4fAAX7yk59QWlrK4MGDGTZsGO+991503LXXXsvYsWOjh65Fkpmx1jqy4MLCQltSUpKw+RUXF1NUVJSw+UnDWuu6XrNmDQMGDGiy5ZWVlZGdnd1ky2tuJk+ezF133cX48eMbrJOIv0lr/Tw3Na3nMGPMp9bawrrl2jMWkWblwIED9OvXj/T09GMGsUgy0QVcIkmsJT7PuE2bNqxfv97pZog0KYWxSBLT84xFWgYdphY5Dk5dYyFH099CkonCWCROaWlp7Nu3TyHQDFhr2bdvH2lpaU43RSQhdJhaJE5du3Zl27Zt7Nmzp0mW5/P5FDbHkJaWRteuXZ1uhkhCxBXGxphJwGOAG/iztfbXdcabyPhLgArgRmvtZwluq4ijvF5v9DaOTaG4uJjhw4c32fJExDmNHqY2xriBx4GLgYHAdcaYgXWqXQz0jbxuAZ5IcDtFRESSVjznjEcDG6y1G6211cB8oO4z2i4H5tqwj4A2xphOCW6riIhIUoonjLsAW2OGt0XKjreOiIiI1COec8b1PcKl7uWk8dTBGHML4cPYAOXGmHVxLD9e7YG9CZyfNEzrumloPTcNreemofUc1qO+wnjCeBvQLWa4K7DjBOpgrX0KeCqOZR43Y0xJfff7lMTTum4aWs9NQ+u5aWg9H1s8h6mXAX2NMb2MMSnAVOC1OnVeA75pws4EDlprdya4rSIiIkmp0T1ja23AGPNd4B3CP216xlq7yhhza2T8k8BbhH/WtIHwT5v0NHAREZE4xfU7Y2vtW4QDN7bsyZh+C3wnsU07bqfk8LfUS+u6aWg9Nw2t56ah9XwMjj3PWERERMJ0b2oRERGHJUUYG2MmGWPWGWM2GGN+5HR7kpExppsx5j1jzBpjzCpjzJ1OtymZGWPcxph/GWPecLotycwY08YY85IxZm3ks32W021KRsaYuyLfGyuNMS8YY3TT9TpafBjHebtOOXkB4PvW2gHAmcB3tJ5PqTuBNU43ohV4DHjbWtsfGIbWecIZY7oAdwCF1trBhC8Enupsq5qfFh/GxHe7TjlJ1tqdNQ//sNaWEf7S0l3WTgFjTFfgG8CfnW5LMjPG5ADjgP8FsNZWW2sPONuqpOUB0o0xHiCDeu5D0dolQxjrVpxNzBjTExgOfOxsS5LW/wN+CIScbkiS6w3sAWZFTgn82RiT6XSjko21djvwCLAF2En4PhSLnG1V85MMYRzXrTglMYwxWcDLwPestYecbk+yMcZMBr621n7qdFtaAQ8wAnjCWjscOAzompMEM8bkET5a2QvoDGQaY25wtlXNTzKEcVy34pSTZ4zxEg7iedbahU63J0mNBS4zxmwifMrlAmPMc842KWltA7ZZa2uO8LxEOJwlsS4EvrLW7rHW+oGFwNkOt6nZSYYwjud2nXKSjDGG8Lm1Ndba3zjdnmRlrb3PWtvVWtuT8Gf5XWut9iJOAWvtLmCrMeb0SNF4YLWDTUpWW4AzjTEZke+R8ehCuaPEdQeu5qyh23U63KxkNBaYDqwwxiyPlP04cnc2kZbqdmBeZEN+I7qVb8JZaz82xrwEfEb4Vxn/QnfjOoruwCUiIuKwZDhMLSIi0qIpjEVERBymMBYREXGYwlhERMRhCmMRERGHKYxFREQcpjAWERFxmMJYRETEYf8/qB5op+nMeckAAAAASUVORK5CYII=)

输出结果所示，accuracy和val_accuracy呈稳步增长趋势，而loss和val_loss呈下降趋势



# **标准化**

数据标准化处理，只需要修改两个部分，一是在引入数据集后进行标准化处理，二是修改训练模型的参数即可。



## **1.对数据进行标准化处理的代码**

```
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
x_train_scaled = scaler.fit_transform(x_train.astype(np.float32).reshape(-1,28*28)).reshape(-1,28,28)

x_valid_scaled = scaler.transform(x_valid.astype(np.float32).reshape(-1,28*28)).reshape(-1, 28, 28)

x_test_scaled = scaler.transform(x_test.astype(np.float32).reshape(-1,28*28)).reshape(-1, 28, 28)
```



## 2.修改训练模型的参数

x_train变为x_train_scaled，x_valid变为x_valid_scaled即可。

```
#构建好图以后，可以开启训练
history = model.fit(x_train_scaled, y_train, epochs=10,
          #epochs表示命令这个训练集多少次，epochs=10表示命令这个训练集10次
         validation_data=(x_valid_scaled,y_valid)
          #每隔段时间，会对这个训练集做验证 
         )

#history表示model.fit返回的一个值，称为 数据结果
```



# 回调函数

此处主要是TensorBoard、EarlyStopping、ModelCheckPoint三种回调函数。

- TensorBoard：是Tensorflow自带的一个强大的可视化工具，也是一个web应用程序套件，通过将tensorflow程序输出的日志文件的信息可视化使得tensorflow程序的理解、调试和优化更加简单高效。Tensorboard的可视化依赖于tensorflow程序运行输出的日志文件，因而tensorboard和tensorflow程序在不同的进程中运行。
  参数详解：https://tensorflow.google.cn/api_docs/python/tf/keras/callbacks/TensorBoard


- EarlyStopping：为了获得性能良好的神经网络，网络定型过程中需要进行许多关于所用设置（超参数）的决策。超参数之一是定型周期（epoch）的数量：亦即应当完整遍历数据集多少次（一次为一个epoch？如果epoch数量太少，网络有可能发生欠拟合（即对于定型数据的学习不够充分）；如果epoch数量太多，则有可能发生过拟合（即网络对定型数据中的“噪声”而非信号拟合）。早停法旨在解决epoch数量需要手动设置的问题。它也可以被视为一种能够避免网络发生过拟合的正则化方法（与L1/L2权重衰减和丢弃法类似）。根本原因就是因为继续训练会导致测试集上的准确率下降。那继续训练导致测试准确率下降的原因猜测可能是：

  1. 过拟合

  2. 学习率过大导致不收敛。

     参数详解：https://tensorflow.google.cn/api_docs/python/tf/keras/callbacks/EarlyStopping

     monitor: 需要监视的量，val_loss，val_acc。
     patience: 当early stop被激活(如发现loss相比上一个epoch训练没有下降)，则经过patience个epoch后停止训练。
     mode: ‘auto’,‘min’,'max’之一，在min模式训练，如果检测值停止下降则终止训练。在max模式下，当检测值不再上升的时候则停止训练。
     min_delta：阈值。

- ModelCheckPoint：在每次迭代之后保存模型。



使用方式：

**Callback是在训练过程中调用，因此我们要在模型训练（model.fit）中添加**

先定义回调函数，然后在fit()方法中添加。

```
import os
#使用回调函数
#tensorBoard、EarlyStopping、ModelCheckPoint
logdir = os.path.join("callbacks")

if not os.path.exists(logdir):
    os.mkdir(logdir)
output_model_file = os.path.join(logdir,"fashion_mnist_model.h5")


callbacks=[
     keras.callbacks.TensorBoard(log_dir=logdir), #log_dir将输出的日志保存在所要保存的路径中
     keras.callbacks.ModelCheckpoint(output_model_file, save_best_only = True), 
    keras.callbacks.EarlyStopping(patience=5, min_delta=1e-3),
    
]
```

结果展示：

![image-20210504154003174](README.assets/image-20210504154003174.png)

运行后生成的文件，callbacks为tensorboard生成的文件夹，fashion_mnist_model为modelcheckpoint生成的文件，这样我们就可以查看相应的文件了。





# 实战-回归问题

## 1.导包


```
import matplotlib as mpl
import matplotlib.pyplot as plt
#直接在python console里面生成图像
%matplotlib inline 
import numpy as np
import sklearn
import pandas as pd
from tensorflow import keras


for module in mpl, np, sklearn, pd:
    print(module.__name__, module.__version__)
```

    matplotlib 3.2.2
    numpy 1.19.5
    sklearn 0.23.1
    pandas 1.0.5


## 2.获取数据集


```
from sklearn.datasets import fetch_california_housing
housing = fetch_california_housing()
print(housing.DESCR)
print(housing.data.shape)
print(housing.target.shape)

```

    .. _california_housing_dataset:
    
    California Housing dataset
    --------------------------
    
    **Data Set Characteristics:**
    
        :Number of Instances: 20640
    
        :Number of Attributes: 8 numeric, predictive attributes and the target
    
        :Attribute Information:
            - MedInc        median income in block
            - HouseAge      median house age in block
            - AveRooms      average number of rooms
            - AveBedrms     average number of bedrooms
            - Population    block population
            - AveOccup      average house occupancy
            - Latitude      house block latitude
            - Longitude     house block longitude
    
        :Missing Attribute Values: None
    
    This dataset was obtained from the StatLib repository.
    http://lib.stat.cmu.edu/datasets/
    
    The target variable is the median house value for California districts.
    
    This dataset was derived from the 1990 U.S. census, using one row per census
    block group. A block group is the smallest geographical unit for which the U.S.
    Census Bureau publishes sample data (a block group typically has a population
    of 600 to 3,000 people).
    
    It can be downloaded/loaded using the
    :func:`sklearn.datasets.fetch_california_housing` function.
    
    .. topic:: References
    
        - Pace, R. Kelley and Ronald Barry, Sparse Spatial Autoregressions,
          Statistics and Probability Letters, 33 (1997) 291-297
    
    (20640, 8)
    (20640,)



```
#换一种数据打印方式
import pprint
pprint.pprint(housing.data[0:5])
pprint.pprint(housing.target[0:5])
```

    array([[ 8.32520000e+00,  4.10000000e+01,  6.98412698e+00,
             1.02380952e+00,  3.22000000e+02,  2.55555556e+00,
             3.78800000e+01, -1.22230000e+02],
           [ 8.30140000e+00,  2.10000000e+01,  6.23813708e+00,
             9.71880492e-01,  2.40100000e+03,  2.10984183e+00,
             3.78600000e+01, -1.22220000e+02],
           [ 7.25740000e+00,  5.20000000e+01,  8.28813559e+00,
             1.07344633e+00,  4.96000000e+02,  2.80225989e+00,
             3.78500000e+01, -1.22240000e+02],
           [ 5.64310000e+00,  5.20000000e+01,  5.81735160e+00,
             1.07305936e+00,  5.58000000e+02,  2.54794521e+00,
             3.78500000e+01, -1.22250000e+02],
           [ 3.84620000e+00,  5.20000000e+01,  6.28185328e+00,
             1.08108108e+00,  5.65000000e+02,  2.18146718e+00,
             3.78500000e+01, -1.22250000e+02]])
    array([4.526, 3.585, 3.521, 3.413, 3.422])


## 3.数据预处理

### ①数据划分


```
"""

train_test_split()函数是用来随机划分样本数据为训练集和测试集的，当然也可以人为的切片划分。

优点：随机客观的划分数据，减少人为因素

完整模板：

train_X,test_X,train_y,test_y = train_test_split(train_data,train_target,test_size=0.3,random_state=5)

参数解释：

train_data：待划分样本数据

train_target：待划分样本数据的结果（标签）

test_size：测试数据占样本数据的比例，若整数则样本数量

random_state：设置随机数种子，保证每次都是同一个随机数。若为0或不填，则每次得到数据都不一样

"""

from sklearn.model_selection import train_test_split

x_train_all, x_test, y_train_all, y_test = train_test_split(
    housing.data, housing.target, random_state = 7)

x_train, x_valid, y_train, y_valid = train_test_split(
    x_train_all, y_train_all, random_state = 11)

print(x_train.shape, y_train.shape)
print(x_valid.shape, y_valid.shape)
print(x_test.shape, y_test.shape)
```

    (11610, 8) (11610,)
    (3870, 8) (3870,)
    (5160, 8) (5160,)


### ②数据归一化处理


```
from sklearn.preprocessing import StandardScaler

# 标准化数据，保证每个维度的特征数据方差为1，均值为0，使得预测结果不会被某些维度过大的特征值而主导  
scaler=StandardScaler()

"""

transform函数是一定可以替换为fit_transform函数的，fit_transform函数不能替换为transform函数！
fit_transform()先拟合数据，再标准化  
transform()数据标准化

"""

x_train_scaled = scaler.fit_transform(x_train)
x_valid_scaled = scaler.transform(x_valid)
x_test_scaled  = scaler.transform(x_test)

"""
在训练集上调用fit_transform()，其实找到了均值μ和方差σ^2，即我们已经找到了转换规则，
我们把这个规则利用在训练集上，同样，我们可以直接将其运用到测试集上（甚至交叉验证集），
所以在测试集上的处理，我们只需要标准化数据而不需要再次拟合数据

即一次fit_transform，多次transform。

"""

```

![image-20210506095916741](README.assets/image-20210506095916741.png)

## 4.构建模型


```
model = keras.models.Sequential([
    keras.layers.Dense(30, activation='relu',
                       input_shape=x_train.shape[1:]),
    keras.layers.Dense(1),
])

model.compile(loss="mean_squared_error", optimizer="sgd")

```

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    dense (Dense)                (None, 30)                270       
    _________________________________________________________________
    dense_1 (Dense)              (None, 1)                 31        
    =================================================================
    Total params: 301
    Trainable params: 301
    Non-trainable params: 0
    _________________________________________________________________

查看模型中的层数


```
model.layers
```


    [<tensorflow.python.keras.layers.core.Dense at 0x20b5fd55370>,
     <tensorflow.python.keras.layers.core.Dense at 0x20b5fd75d00>]

输出模型各层的参数状况


```
model.summary()
```

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    dense (Dense)                (None, 30)                270       
    _________________________________________________________________
    dense_1 (Dense)              (None, 1)                 31        
    =================================================================
    Total params: 301
    Trainable params: 301
    Non-trainable params: 0
    _________________________________________________________________


## 5.定义回调函数


```
callbacks = [keras.callbacks.EarlyStopping(
    patience=5, min_delta=1e-2)]
```

## 6.模型训练


```
history = model.fit(x_train_scaled, y_train,
                     validation_data = (x_valid_scaled, y_valid),
                     epochs = 100,
                    callbacks = callbacks
                   )
```

    Epoch 1/100
    363/363 [==============================] - 4s 3ms/step - loss: 1.4136 - val_loss: 0.6233
    Epoch 2/100
    363/363 [==============================] - 1s 1ms/step - loss: 0.5406 - val_loss: 0.5268
    Epoch 3/100
    363/363 [==============================] - 0s 1ms/step - loss: 0.4971 - val_loss: 0.5025
    Epoch 4/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.4556 - val_loss: 0.5557
    Epoch 5/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.4415 - val_loss: 0.4389
    Epoch 6/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.4089 - val_loss: 0.4311
    Epoch 7/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.4028 - val_loss: 0.4197
    Epoch 8/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.3972 - val_loss: 0.4153
    Epoch 9/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.3880 - val_loss: 0.4121
    Epoch 10/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.3776 - val_loss: 0.4003
    Epoch 11/100
    363/363 [==============================] - 1s 1ms/step - loss: 0.3818 - val_loss: 0.4015
    Epoch 12/100
    363/363 [==============================] - 0s 1ms/step - loss: 0.3780 - val_loss: 0.4000
    Epoch 13/100
    363/363 [==============================] - 0s 1ms/step - loss: 0.3861 - val_loss: 0.3882
    Epoch 14/100
    363/363 [==============================] - 0s 1ms/step - loss: 0.3761 - val_loss: 0.3844
    Epoch 15/100
    363/363 [==============================] - 0s 1ms/step - loss: 0.3629 - val_loss: 0.3849
    Epoch 16/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.3754 - val_loss: 0.3805
    Epoch 17/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.3723 - val_loss: 0.3808
    Epoch 18/100
    363/363 [==============================] - 0s 1ms/step - loss: 0.3502 - val_loss: 0.3746
    Epoch 19/100
    363/363 [==============================] - 0s 1ms/step - loss: 0.4067 - val_loss: 0.3780
    Epoch 20/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.3649 - val_loss: 0.3759
    Epoch 21/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.3649 - val_loss: 0.3720
    Epoch 22/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.3637 - val_loss: 0.3701
    Epoch 23/100
    363/363 [==============================] - 1s 2ms/step - loss: 0.3556 - val_loss: 0.3693

history中的一个变量，其中包含一些值


```
history.history
```

效果为：


    {'loss': [0.8632075786590576,
      0.5363500118255615,
      0.49669119715690613,
      0.4485248029232025,
      0.43020644783973694,
      0.4132368266582489,
      0.4070609509944916,
      0.3976878821849823,
      0.3911682963371277,
      0.3853471875190735,
      0.3808273673057556,
      0.37864023447036743,
      0.38333484530448914,
      0.37331584095954895,
      0.36974960565567017,
      0.37044206261634827,
      0.37571442127227783,
      0.3681437373161316,
      0.38591858744621277,
      0.3662325143814087,
      0.35985830426216125,
      0.3577727675437927,
      0.3559771776199341],
     'val_loss': [0.6232901215553284,
      0.5267685055732727,
      0.5024823546409607,
      0.5556734800338745,
      0.43892285227775574,
      0.4310579299926758,
      0.41965991258621216,
      0.415304571390152,
      0.4120640754699707,
      0.40032127499580383,
      0.40154874324798584,
      0.4000304937362671,
      0.3881605267524719,
      0.38438355922698975,
      0.3849121332168579,
      0.3804555833339691,
      0.3808414340019226,
      0.37457671761512756,
      0.37804263830184937,
      0.375925213098526,
      0.37196823954582214,
      0.3701278269290924,
      0.3693447411060333]}



## 7.可视化


```
def plot_learning_curves(history):
    pd.DataFrame(history.history).plot(figsize=(8, 5))
    plt.grid(True)
    plt.gca().set_ylim(0,1)
    plt.show()
    
plot_learning_curves(history)
    
```

效果为：

![png](README.assets/output_23_0.png)


## 8.测试


```
"""
evaluate 中的 verbose
verbose：日志显示
verbose = 0 为不在标准输出流输出日志信息
verbose = 1 为输出进度条记录
注意： 只能取 0 和 1；默认为 1
"""

model.evaluate(x_test_scaled, y_test, verbose=1)
```

    162/162 [==============================] - 0s 764us/step - loss: 0.3758
    
    0.37575313448905945



